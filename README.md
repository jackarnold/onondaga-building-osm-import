Python scripts used to simplify building footprint shapefile data from the Syracuse-Onondaga County Planning Agency for import into OpenStreetMap. 

See the status of this import project here: https://wiki.openstreetmap.org/wiki/Import/Catalogue/Onondaga_County_NY_Building_Footprint_Import

# Setup
1. Download and install Python 3, which is available from here: https://www.python.org/downloads/
2. Clone this repository
3. Open a Terminal/Command Prompt in the directory containing this repository
4. (Optional) It is recommended to use a virtual environment: `python3 -m venv venv`
5. Install the required libaries using pip: `pip install -r requirements.txt`
6. Run the script: `python3 prepare.py`
7. The output log and prepared import files are now in the `output`, in the directory corresponding to todays date
