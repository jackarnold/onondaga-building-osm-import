import sys
import os
import logging
from datetime import datetime

import requests
import osm2geojson
import geopandas as gpd

#
##
#

# Log the number of elements in dataframe
def log_elements(df):
	logging.info('The dataframe has %i elements' % len(df.index))

# Query Overpass for OSM buildings inside bound
def query_overpass(bound):
	# Reorder bounds and add margin to bound
	bound_conv = (bound[1] - BOUND_BUFFER, bound[0] - BOUND_BUFFER, bound[3] + BOUND_BUFFER, bound[2] + BOUND_BUFFER)
	query = OVERPASS_QUERY % (str(bound_conv), str(bound_conv))
	r = requests.get(OVERPASS_URL, params={'data': query})
	j = osm2geojson.json2geojson(r.json())
	gdf = gpd.GeoDataFrame.from_features(j).set_crs('epsg:4326')
	return gdf

def features_within_feature(feature, buildings):
	# Select buildings completely WITHIN feature
	muni_mask_within = buildings.within(feature)
	# Select buildings that OVERLAP the feature
	muni_mask_overlap = buildings.loc[~muni_mask_within].overlaps(feature)
	
	muni_mask = muni_mask_within
	# Add buildings with intersection area >= 50% to mask
	for index in muni_mask_overlap[muni_mask_overlap].index:
		building = buildings.iloc[index]
		intersection = building.geometry.intersection(feature)
		percent = intersection.area / building.geometry.area
		if percent >= 0.5:
			muni_mask.iloc[index] = True
	muni_buildings = buildings.loc[muni_mask]
	return muni_buildings

#
##
#

# Configuration
OUTPUT_DIR = './output/'
SOURCE_DIR = './source/'
SOURCE_BUILDINGS_FILE = SOURCE_DIR + 'onondaga_county_building_footprints/onondaga_county_building_footprints.shp'
SOURCE_MUNICIPALITIES_FILE = SOURCE_DIR + 'municipalities/municipalities.shp'

OVERPASS_URL = 'https://overpass-api.de/api/interpreter'
OVERPASS_QUERY = '[out:json][timeout:25];(way[~"building"~".*"]%s;relation[~"building"~".*"]%s;);out body;>;out skel qt;'
BOUND_BUFFER = 0.1 # ~0.5 mile

MUNICIPALITIES_SELECT = None

#
##
#

# Setup output directories
now = datetime.now().strftime('%Y%m%d-%H%M%S')
OUTPUT_DIR += now + '/'

os.makedirs(OUTPUT_DIR)
for directory in ['openstreetmap', 'buildings', 'buildings_conflict']:
	os.makedirs(OUTPUT_DIR + directory)

# Log to STDOUT and log file
logging.basicConfig(
	level=logging.INFO,
	format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
	handlers=[
		logging.FileHandler(OUTPUT_DIR + 'output.log'),
		logging.StreamHandler()
	]
)

# Open the buildings shapefile
buildings = gpd.read_file(SOURCE_BUILDINGS_FILE)
logging.info('Opened the buildings shapefile')
log_elements(buildings)

# Convert the buildings shapefile
buildings = buildings.to_crs('epsg:4326')
logging.info('Converted the dataframe CRS')
log_elements(buildings)

# Open the municipalities shapefile
municipalities = gpd.read_file(SOURCE_MUNICIPALITIES_FILE)
logging.info('Opened the municipalities shapefile')
log_elements(municipalities)

# Dissolve municipalities with the same names
municipalities = municipalities.dissolve(by='MUNI', as_index=False)
logging.info('Dissolved municipalities dataframe')
log_elements(municipalities)

#
##
#

# Iterate over munipalities
for municipality in municipalities.itertuples():
	# Continue if select municipality
	if MUNICIPALITIES_SELECT and municipality.MUNI not in MUNICIPALITIES_SELECT:
		continue
	logging.info('Selected municipality: %s' % municipality.MUNI)
	
	# Get buildings within municipality
	muni_buildings = features_within_feature(municipality.geometry, buildings)
	logging.info('Extracted buildings within municipality')
	log_elements(muni_buildings)
	
	# Get buildings from OpenStreetMap (Overpass) within municipality
	bound = municipality.geometry.bounds
	overpass_buildings = query_overpass(bound)
	osm_buildings = features_within_feature(municipality.geometry, overpass_buildings)
	osm_buildings = osm_buildings[['geometry']]
	logging.info('Get buildings from OpenStreetMap within municipality')
	log_elements(osm_buildings)
	
	# Find intersecting buildings between two sets
	muni_in_osm_buildings = gpd.sjoin(muni_buildings, osm_buildings, how='inner', op='intersects')
	muni_in_osm_buildings = muni_in_osm_buildings[['geometry']]
	logging.info('Merged dataframes with intersection')
	log_elements(muni_in_osm_buildings)
	
	# Get the non-intersecting buildings (inverse set)
	notosm_mask = muni_buildings.index.isin(muni_in_osm_buildings.index)
	muni_not_in_osm_buildings = muni_buildings[~notosm_mask]
	muni_not_in_osm_buildings = muni_not_in_osm_buildings[['geometry']]
	muni_not_in_osm_buildings['building'] = 'yes'
	logging.info('Extracted buildings not intersecting')
	log_elements(muni_not_in_osm_buildings)
	
	# Export non-empty shapefiles
	filename = str(municipality.Index) + '_' + municipality.MUNI
	if not osm_buildings.empty:
		osm_buildings.to_file(OUTPUT_DIR + 'openstreetmap/' + filename + '.geojson', driver="GeoJSON")
	if not muni_not_in_osm_buildings.empty:
		muni_not_in_osm_buildings.to_file(OUTPUT_DIR + 'buildings/' + filename + '.geojson', driver="GeoJSON")
	if not muni_in_osm_buildings.empty:
		muni_in_osm_buildings.to_file(OUTPUT_DIR + 'buildings_conflict/' + filename + '.geojson', driver="GeoJSON")
	logging.info('Exported shapefiles for municipality')
	
	
